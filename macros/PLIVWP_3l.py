#!/usr/bin/env python

import os
import socket
import sys
import time
import logging
import math

import h5py
import numpy as np
import pandas as pd

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from joblib import Parallel,delayed

from optparse import OptionParser
p = OptionParser()

p.add_option('-d', '--debug',    action='store_true', default=False)
p.add_option('--plot',           action='store_true', default=False)
p.add_option('--plotDist',       action='store_true', default=False)
p.add_option('--do-sys30',       action='store_true', default=False)
p.add_option('--do-sys50',       action='store_true', default=False)
p.add_option('--do-wh',          action='store_true', default=False)
p.add_option('--do-ssww',        action='store_true', default=False)
p.add_option('--do-tth',         action='store_true', default=False)
p.add_option('--lep-veto',       action='store_true', default=False)

p.add_option('-n', '--nevent',   type='int',    default=None)
p.add_option('-o', '--outdir',   type='string', default='models/')
p.add_option('--outkey',         type='string', default='mlp')
p.add_option('--scaler',         type='string', default=None)

p.add_option('--highpt',         type='int',    default=None)

(options,args) = p.parse_args()

#======================================================================================================        
def getLog(name, level='INFO', debug=False, print_time=False):

    if print_time:
        f = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
    else:
        f = logging.Formatter('%(message)s')
        
    sh = logging.StreamHandler()
    sh.setFormatter(f)

    logging._warn_preinit_stderr = 0
    
    log = logging.getLogger(name)

    log.handlers  = []
    log.propagate = False
    
    log.addHandler(sh)
    
    if debug:
        log.setLevel(logging.DEBUG)
    else:
        if level == 'DEBUG':   log.setLevel(logging.DEBUG)
        if level == 'INFO':    log.setLevel(logging.INFO)
        if level == 'WARNING': log.setLevel(logging.WARNING)    
        if level == 'ERROR':   log.setLevel(logging.ERROR)

    return log

#======================================================================================================        
log = getLog(os.path.basename(__file__), print_time=False)

#======================================================================================================
def getOutName(name):
    
    if not options.outdir:
        return None
    
    outdir = '%s/' %(options.outdir.rstrip('/'))
    
    if options.highpt:
        outdir = outdir + "Pt%d"%options.highpt 

    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    return '%s/%s' %(outdir.rstrip('/'), name)

#======================================================================================================        
def getEffPLIVCut(lep_data, eff, pT, isleft=False, binsize=1000):
    index = [False]*len(lep_data)

    if isleft:
        index = (lep_data['pt'] > (pT-binsize)) & (lep_data['pt'] < pT)
    else:
        index = (lep_data['pt'] < (pT+binsize)) & (lep_data['pt'] > pT)

    #lep_data[index]['pliv'][lep_data[index]['pliv'] > -1.1].plot(kind='hist', bins=200)
    pliv_list = lep_data[index]['pliv'].values
    total_leps_bin = len(pliv_list)
    
    id_of_eff = round(total_leps_bin*eff)
    
    return np.sort(pliv_list)[id_of_eff]
    
    
#====================================================================================================== 
class Bin:
    def __init__(self, leps, bin_left, left_eff, bin_right=100e3, right_eff=None):
        self.bin_left  = bin_left
        self.left_eff  = left_eff
        
        self.bin_right = bin_right
        self.right_eff = right_eff
        
        # cut pliv value
        self.left_cut = getEffPLIVCut(leps, left_eff, bin_left, isleft=False, binsize=1e3)
        
        if bin_right and right_eff:
            self.right_cut = getEffPLIVCut(leps, right_eff, bin_right, isleft=True, binsize=1e3)
        else:
            self.right_cut = self.left_cut
            
    
    def getPLIVCut(self, pT):
        k = (self.right_cut - self.left_cut)/(self.bin_right - self.bin_left)
        b = self.right_cut - self.bin_right*k
        
        return k*pT+b
    
    def isInside(self, pT):
        if pT >= self.bin_left and pT < self.bin_right:
            return True
        
        return False
        

#====================================================================================================== 
class PLIVWP:
    def __init__(self, leps, edges, effs):
        if len(edges) != len(effs):
            raise("Unmatched edges size and effs size")
        
        self.bins = []
        for i in range(len(edges)):
            if i+1 >= len(edges):
                cur_bin = Bin(leps, edges[i], effs[i])
            else:
                cur_bin = Bin(leps, edges[i], effs[i], edges[i+1], effs[i+1])
            
            self.bins += [cur_bin]
        
        self.maxEdge = edges[len(edges) -1]
            
        
    def getFullPLIVCut(self, pT):
        if pT >= self.maxEdge:
            return self.bins[-1].getPLIVCut(pT)
        
        for cur_bin in self.bins:
            if cur_bin.isInside(pT):
                return cur_bin.getPLIVCut(pT)
        
        return 0
#====================================================================================================== 
def getFineTuneEffsList(effs_cand_list):
    effs_list = []

    for i in effs_cand_list[0]:
        for j in effs_cand_list[1]:
            if j <= i: continue
            for k in effs_cand_list[2]:
                if k <= j: continue
                for l in effs_cand_list[3]:
                    if l<= k: continue
                    for m in effs_cand_list[4]:
                        if m <= l: continue
                        for n in effs_cand_list[5]:
                            if n < m: continue
                            effs_list += [[i, j, k, l, m, n]]

    return effs_list

     
#====================================================================================================== 
def getEffsList(effs_cand, highpt=False):
    effs_list = []
    
    if options.highpt or highpt:
        for k in range(len(effs_cand)):
            for l in range(len(effs_cand)):
                if l<= k: continue
                for m in range(len(effs_cand)):
                    if m <= l: continue
                    for n in range(len(effs_cand)):
                        if n < m: continue
                        effs_list += [[effs_cand[0], effs_cand[1], effs_cand[k], effs_cand[l], effs_cand[m], effs_cand[n]]]
    
    else:
        for i in range(len(effs_cand)):
            for j in range(len(effs_cand)):
                if j <= i: continue
                for k in range(len(effs_cand)):
                    if k <= j: continue
                    for l in range(len(effs_cand)):
                        if l<= k: continue
                        for m in range(len(effs_cand)):
                            if m <= l: continue
                            for n in range(len(effs_cand)):
                                if n < m: continue
                                effs_list += [[effs_cand[i], effs_cand[j], effs_cand[k], effs_cand[l], effs_cand[m], effs_cand[n]]]

    return effs_list

#====================================================================================================== 
def passIso(event_leps, iso_index):
    lep0_pass = (event_leps[:, 0, iso_index] == 1)
    lep1_pass = (event_leps[:, 1, iso_index] == 1)
    lep2_pass = (event_leps[:, 2, iso_index] == 1)

    return (lep0_pass & lep1_pass & lep2_pass)

#====================================================================================================== 
def getNEventPass(event, wp):
    total_evt = event['event_weight'].sum()
    
    lep0_pliv_cut = event['Lep0Pt'].apply(wp.getFullPLIVCut)
    lep1_pliv_cut = event['Lep1Pt'].apply(wp.getFullPLIVCut)
    lep2_pliv_cut = event['Lep2Pt'].apply(wp.getFullPLIVCut)
    
    pass_pliv_idx = (event['Lep0PLIV'] < lep0_pliv_cut) & (event['Lep1PLIV'] < lep1_pliv_cut ) & (event['Lep2PLIV'] < lep2_pliv_cut)
    
    return event[pass_pliv_idx]['event_weight'].sum(), event[pass_pliv_idx]['event_weight'].sum()/total_evt
        

#======================================================================================================        
def plotWPEffRej(lep_data, pliv_wp, name="PLIV_WP", isos=[], ptcuts=None, useShortName=False):
    if not ptcuts:
        pt_edges = [10e3+i*2e3 for i in range(46)]
    else:
        pt_edges = ptcuts

    if options.highpt:
        new_pt_edges = []
        for pt_edge in pt_edges:
            if pt_edge < options.highpt*1e3: continue
            new_pt_edges += [pt_edge]

        pt_edges = new_pt_edges

    lep_pliv_cuts = lep_data['pt'].apply(pliv_wp.getFullPLIVCut)
    # build helper lepton objects
    helper_lep_eff = pd.DataFrame()
    helper_lep_eff['pt'] = lep_data['pt'] 
    helper_lep_eff['PassPLIV'] = lep_data['pliv'] < lep_pliv_cuts

    split_data = pd.cut(helper_lep_eff['pt'], pt_edges + [10e5])
    prompt_effs = helper_lep_eff['PassPLIV'].groupby(split_data).sum()/helper_lep_eff['PassPLIV'].groupby(split_data).count()
    yerrs = getEffErrors(helper_lep_eff['PassPLIV'].groupby(split_data).sum(), helper_lep_eff['PassPLIV'].groupby(split_data).count())

    plt.clf()
    fig, ax = plt.subplots()
    ax.errorbar(pt_edges, prompt_effs.values, yerr=yerrs, label="PLIV")

    lep_data_group = lep_data.groupby(split_data)
    for iso in isos:
        isoeffs = lep_data_group[iso].sum()/lep_data_group[iso].count()
        isoerrs = getEffErrors(lep_data_group[iso].sum(), lep_data_group[iso].count())
        ax.errorbar(pt_edges, isoeffs.values, yerr=isoerrs, label=iso)
    
    ax.legend(loc='best')
    ax.set(xlabel='pT [MeV]', ylabel='efficiency')

    plt.savefig('%s_eff.pdf' %getOutName(name), format='pdf')

    if useShortName:
        plt.savefig('%s_eff.pdf' %getOutName(name.split('_')[0] + '_' + name.split('_')[-1]), format='pdf')


#======================================================================================================        
def plotPLIVCut(pliv_wp, name="PLIV_WP", useShortName=False):
    pts = np.arange(10e3, 100e3, 1e3)
    pliv_cuts = []
   
    for pt in pts:
        pliv_cut = pliv_wp.getFullPLIVCut(pt)
        
        if pliv_cut < -1.1:
            pliv_cut = -1

        pliv_cuts += [pliv_cut]

    plt.clf()
    fig, ax = plt.subplots()
    ax.plot(pts, pliv_cuts)
    ax.plot(pts, pliv_cuts, marker='.')
    ax.set(xlabel='pT [MeV]', ylabel='PLIV cut value')

    plt.savefig('%s_cut.pdf' %getOutName(name), format='pdf')

    if useShortName:
        plt.savefig('%s_cut.pdf' %getOutName(name.split('_')[0]), format='pdf') 


#======================================================================================================        
def printOutTable(out, name="PLIVWP_table", ):
    if options.do_wh:
        text = """\\resizebox{1.0\\textwidth}{!}{
            \\begin{tabular}{l||r|r|r|r|r|r}
            WPs & significance & wh eff. & n wh & n bkgs & n ttbar & n wz  \\\\
            \\hline \n"""
    elif options.do_ssww:
        text = """\\resizebox{1.0\\textwidth}{!}{
            \\begin{tabular}{l||r|r|r|r|r|r|r}
            WPs & significance & ssWW eff. & n ssWW & n bkgs & n ttbar & n wz & n QCD ssWW \\\\
            \\hline \n"""

    else:
        text = """\\resizebox{1.0\\textwidth}{!}{
            \\begin{tabular}{l||r|r|r|r|r|r|r}
            WPs & significance & ttH eff. & n ttH & n bkgs & n ttbar & n wz & n ttW \\\\
            \\hline \n"""
    for wp, signi, sigeff, nsig, nbkg, nbkgs in out:
        wp_name = '\\_'.join(wp.split('_'))
        text += wp_name +'& %20s& %20s& %20s& %20s'%('%f'%signi,'%f'%sigeff, '%f'%nsig, '%f'%nbkg)

        for nbkg_cur in nbkgs:
            text += '& %20s'%('%f'%nbkg_cur)

        text += '\\\\ \n'
    text += "\end{tabular}} \n "

    ofile = open(getOutName(name)+'.tex', 'w') 
    ofile.write(text)
    ofile.close()

#======================================================================================================        
def plotPLIVSum(out, name="PLIVWP_sum"):
    signis  = []
    sigeffs = []
    bkgeffs = []

    for wp, signi, sigeff, nsig, nbkg, _ in out:
#        if bkgeff == 0: signi = 0

        signis  += [signi] 
        sigeffs += [sigeff]
#        bkgeffs += [bkgeff]

    var_name = ['significance', 'tth eff.']
    var_data = [signis, sigeffs, bkgeffs]

    for i in range(len(var_name)):
    
        plt.clf()
        fig, ax = plt.subplots()
        phd = plt.hist(var_data[i], bins=50, histtype='step', color='blue', alpha=0.75)
    
        plt.xlabel(var_name[i])
        plt.ylabel('N PLIV WPs')
        plt.grid(True)
    
        plt.savefig('%s.pdf' %getOutName(name + '_' + var_name[i].replace(' eff.', '_eff')), format='pdf')
             
#======================================================================================================        
def getSignificance(nsig, nbkg, nttbar, nwz):
    if options.do_sys30:
        return float(nsig)/(nbkg + (nttbar*0.3)**2 + (nwz*0.1)**2)**0.5 

    elif options.do_sys50:
        return float(nsig)/(nbkg + (nttbar*0.5)**2 + (nwz*0.1)**2)**0.5 
    else:
        return float(nsig)/(nbkg**0.5)

#======================================================================================================        
def getEffErrors(sig, bkg):
        return(sig/bkg**2 + sig**2/bkg**3)**0.5

#======================================================================================================        
def getIsoOut(sig, bkgs, iso):
    ntotal_sig = sig['event_weight'].sum()

    nsig = sig[sig[iso]]['event_weight'].sum()
    sigeff = nsig/ntotal_sig

    nbkg = 0
    nbkgs = []
    for bkg in bkgs:
        nbkg_curr = bkg[bkg[iso]]['event_weight'].sum()
        nbkg += nbkg_curr
        nbkgs += [nbkg_curr]

    return (iso, getSignificance(nsig, nbkg, nbkgs[0], nbkgs[1]), sigeff, nsig, nbkg, nbkgs)

           
#======================================================================================================        
def CombineTables(tables):
    outs = []

    for i in range(len(tables[0])):        
        wp_name = tables[0][i][0]
        
        comb_signi    = 0
        comb_sig      = 0
        comb_totalsig = 0
        comb_bkg      = 0
        comb_bkgs     = [0]*len(tables[0][i][5]) 

        for table in tables:
            comb_signi    += table[i][1]**2
            comb_totalsig += table[i][3]/table[i][2]
            comb_sig      += table[i][3] 
            comb_bkg      += table[i][4]

            for j in range(len(table[i][5])):
                comb_bkgs[j] += table[i][5][j]
       
        outs += [(wp_name, comb_signi**0.5, float(comb_sig)/comb_totalsig, comb_sig, comb_bkg, comb_bkgs)]
       
    return outs   

#======================================================================================================        
def MainLoop(lep_data, sig, bkgs, edges, effs, sel):
    eff_name = ''
    for eff in effs:
        eff_name += '_%2d'%(eff*100)

    
    pliv_wp = PLIVWP(lep_data, edges, effs)
    
    nsig, sigeff = getNEventPass(sig[sig[sel]], pliv_wp)

    nbkg = 0
    nbkgs = []

    for bkg in bkgs:
        nbkg_curr, _ = getNEventPass(bkg[bkg[sel]], pliv_wp)
        nbkg += nbkg_curr
        nbkgs += [nbkg_curr]

    return ('PLIVWP' + eff_name, getSignificance(nsig, nbkg, nbkgs[0], nbkgs[1]), sigeff, nsig, nbkg, nbkgs)

#======================================================================================================        
def main():

    zjets_file     = args[0]
    tth_ttbar_file = args[1]

    if not os.path.isfile(zjets_file):
        log.warning('Input file does not exist: %s' %zjets_file)
        return    

    if not os.path.isfile(tth_ttbar_file):
        log.warning('Input file does not exist: %s' %tth_ttbar_file)
        return    

    zjets     = h5py.File(zjets_file,     'r')
    tth_ttbar = h5py.File(tth_ttbar_file, 'r')

    log.info('%s contains %d dataset keys' %(zjets, len(zjets.keys())))
    log.info('%s contains %d dataset keys' %(tth_ttbar_file, len(tth_ttbar.keys())))

    maxl = len(max(tth_ttbar.keys(), key=len))
    
    for k in tth_ttbar.keys():
        d = tth_ttbar[k]        
        log.info('   %s len=%6d, size=%6d, dtype=%8s, shape=%s' %(('%s' %k).ljust(maxl), len(d), d.size, d.dtype, d.shape))


    #
    # Prepare DataFrame
    #
    zjets_df = pd.DataFrame(zjets['train_data'][:], columns = zjets['train_vars'][:])
    tth_all_df = pd.DataFrame(tth_ttbar['train_extra_data'][:], columns = tth_ttbar['extra_vars'][:])
    tth_all_df['label'] = tth_ttbar['train_labels'][:]

    # MC weights
    tth_all_df['mc_weight'] = (tth_all_df['MCWeight']
                              *tth_all_df['PUWeight']
                              *tth_all_df['MuonPidSF']
                              *tth_all_df['ElecRecSF']
                              *tth_all_df['MuonTTVASF']
                              *tth_all_df['ElecPidSF']
                              *tth_all_df['JVTWeight']
                              *tth_all_df['FJVTWeight']
                              *tth_all_df['LepTriggerSF']
                              *tth_all_df['bJetReconstructionSF'])

    # xsec weight -- lumi= 138717.96
    if options.do_wh:
        xsec_weight_dict = { 
        341422:3.894841e-02, 
        341424:3.859278e-02, 
        341425:2.054369e-02, 
        341426:3.746339e-02, 
        341428:4.325698e-02, 
        341430:3.882939e-02, 
        341432:4.048026e-02, 
        341433:2.140315e-02, 
        341434:3.780668e-02, 
        341436:4.290887e-02, 
        345326:4.553730e-04, 
        345327:4.318708e-04, 
        345337:5.716804e-04, 
        345446:3.729130e-02, 
        363357:4.614026e-01, 
        363358:2.453311e-01, 
        363489:1.835294e-01, 
        364253:2.477990e-02, 
        364284:9.617265e-04, 
        364289:4.404211e-02, 
        410470:1.979935e-04,}

    elif options.do_ssww:
        xsec_weight_dict = {
        363357:4.614026e-01, 
        363358:2.453311e-01, 
        363489:1.835294e-01, 
        364253:2.477990e-02, 
        410470:1.836485e-04,
        364286:1.275330e-03,
        600078:1.336660e-01, 
        600079:9.982050e-02,
                }

    else:
        xsec_weight_dict = {
            346343:7.026750e-03, 
            346344:3.087892e-03, 
            346345:7.464539e-03, 
            363357:4.614026e-01, 
            363358:2.453311e-01, 
            363489:1.835294e-01, 
            364253:2.477990e-02, 
            410155:5.643760e-03, 
            410470:1.836485e-04,}


    tth_all_df['xsec_weight'] =  tth_all_df['MCChannel'].map(xsec_weight_dict)
    
    # event weight
    tth_all_df['event_weight'] = tth_all_df['xsec_weight']*tth_all_df['mc_weight'] 

    # isolation WPs 
    lep_var_dict = {var: i for i, var in enumerate(tth_ttbar['lep_train_vars'][:])}

    for iso in ['m_leps_isoFCLoose', 'm_leps_isoFCTight', 
                'm_leps_isoFixedCutPflowLoose', 'm_leps_isoFixedCutPflowTight', 
                'm_leps_isoPLVLoose', 'm_leps_isoPLVTight']:
        iso_nick = iso.replace('m_leps_iso', '')
        tth_all_df['pass_'+iso_nick] =  passIso(tth_ttbar['train_lep_data'][:], lep_var_dict[iso])   
    tth_all_df['pass_all'] = len(tth_all_df)*[True]
 
    # Add additional selection:
    selects = []

    if options.do_tth:
        tth_all_df['highpt']  = (tth_all_df['Lep0Pt'] > 20e3) & (tth_all_df['Lep1Pt'] > 20e3) 
        tth_all_df['otherpt'] = ~ tth_all_df['highpt']

        selects += ['highpt', 'otherpt']

    elif options.do_wh:
        selects += ['pass_all']

    elif options.do_ssww:
        tth_all_df['highpt']  = (tth_all_df['Lep0Pt'] > 27e3) & (tth_all_df['Lep1Pt'] > 27e3) 
        tth_all_df['otherpt'] = ~ tth_all_df['highpt']

        selects += ['highpt', 'otherpt']

    isoWPs_list = []
    PLIVWPs_list = []

    for sel in selects:
        isoWPs, out = MakePLIVWPs(zjets_df, tth_all_df, sel)

        isoWPs_list += [isoWPs]
        PLIVWPs_list += [out]

    comb_isoWPs  = CombineTables(isoWPs_list) 
    comb_plivwps = CombineTables(PLIVWPs_list) 

    printOutTable(comb_isoWPs, "IsoWP_table_comb")
    newcomb_isoWPs = sorted(comb_isoWPs, key=lambda significance: significance[1], reverse=True)  
    printOutTable(newcomb_isoWPs, "IsoWP_table_ranked_comb")
    
    newcomb_plivwps = sorted(comb_plivwps, key=lambda significance: significance[1], reverse=True) 
    printOutTable(comb_plivwps, "PLIVWP_table_comb") 
    printOutTable(newcomb_plivwps, "PLIVWP_table_ranked_comb") 

    
    cut_idx = min(len(newcomb_plivwps), 4)
    outforslides = newcomb_plivwps[:cut_idx] + newcomb_isoWPs
    printOutTable(outforslides, "PLIVWP_isoWP_comb")

    plotPLIVSum(newcomb_plivwps, "PLIVWP_sum_comb") 

#======================================================================================================   
def MakePLIVWPs(zjets_df, tth_all_df, sel=None):
    # high pT cut
    if options.highpt:
        index = (tth_all_df['Lep0Pt'] > options.highpt*1e3) & (tth_all_df['Lep1Pt'] > options.highpt*1e3)
        tth_all_df = tth_all_df[index]

    # third lepton veto
    if options.lep_veto:
        index = (tth_all_df['NVetoMuon'] == 0) & (tth_all_df['NVetoElec'] == 0)
        tth_all_df = tth_all_df[index]

    if sel:
        tth_all_df = tth_all_df[tth_all_df[sel]]
  
    # split into different categories
    wz_df    = tth_all_df[tth_all_df['label'] == 0]
    wh_df    = tth_all_df[tth_all_df['label'] == 1]
    tth_df   = tth_all_df[tth_all_df['label'] == 2]
    ttw_df   = tth_all_df[tth_all_df['label'] == 3]
    ttbar_df = tth_all_df[tth_all_df['label'] == 4]
    ssww_df  = tth_all_df[tth_all_df['label'] == 6]

    qcdssww_df = tth_all_df[tth_all_df['label'] == 7]

    if options.do_wh:
        sig_df  = wh_df
        bkg_dfs = [ttbar_df, wz_df] 
    elif options.do_ssww:
        sig_df  = ssww_df
        bkg_dfs = [ttbar_df, wz_df, qcdssww_df] 

    else:
        sig_df  = tth_df
        bkg_dfs = [ttbar_df, wz_df, ttw_df] 

    print("total N ttbar + N tth = ", len(tth_all_df))
    print("N wz    = ",   len(wz_df),      "N wz weighted    = ",   wz_df['event_weight'].sum())
    print("N wh    = ",   len(wh_df),      "N wz weighted    = ",   wh_df['event_weight'].sum())
    print("N tth   = ",   len(tth_df),     "N tth weighted   = ",   tth_df['event_weight'].sum())
    print("N ttw   = ",   len(ttw_df),     "N ttw weighted   = ",   ttw_df['event_weight'].sum())
    print("N ttbar = ",   len(ttbar_df),   "N ttbar weighted = ",   ttbar_df['event_weight'].sum())
    print("N ssww  = ",   len(ssww_df),    "N ssww  weighted = ",   ssww_df['event_weight'].sum())

    print("N qcd ssww  = ",   len(qcdssww_df),    "N qcd ssww  weighted = ",   qcdssww_df['event_weight'].sum())
  
    isoWPs = []
    for iso in [#'FCLoose',
                'FCTight',
                #'FixedCutPflowLoose',
                'FixedCutPflowTight',
                'PLVLoose',
                'PLVTight',
                'all']:

        isoWPs += [getIsoOut(sig_df, bkg_dfs, 'pass_'+iso)]

    printOutTable(isoWPs, "IsoWP_table_"+sel)
    newisoWPs = sorted(isoWPs, key=lambda significance: significance[1], reverse=True)
    printOutTable(newisoWPs, "IsoWP_table_ranked_"+sel)

    
    # prepare prompt leptons from ZJets for PLIV cut value calculation
    lep_data = pd.DataFrame()
    lep_data['pt']   = pd.concat([zjets_df['Lep0Pt'], zjets_df['Lep1Pt']])
    lep_data['pliv'] = pd.concat([zjets_df['Lep0PLIV'], zjets_df['Lep1PLIV']])

    edges = [10e3, 15e3, 20e3, 25e3, 32e3, 43e3]
    
    #effs_cand = [0.75, 0.8, 0.85, 0.9, 0.95, 0.98, 0.99]
    effs_cand = [0.45, 0.48, 0.5, 0.53, 0.55, 0.58, 0.6, 0.63, 0.65, 0.7, 0.75, 0.8, 0.85, 0.88, 0.9, 0.93, 0.95, 0.98]

    #effs_list = getEffsList(effs_cand)
    effs_list = [[0.45, 0.48, 0.59, 0.73, 0.88, 0.97],
                 [0.43, 0.48, 0.50, 0.52, 0.62, 0.91]]

    print("total number of WPs = ", len(effs_list))

    out = Parallel(n_jobs=50)(delayed(MainLoop)(lep_data, sig_df, bkg_dfs, edges, effs, sel) for effs in effs_list)
    #out = []
    #for effs in effs_list:
    #    out += [MainLoop(lep_data, tth_df, ttbar_df, edges, effs)]

    printOutTable(out, "PLIVWP_table_"+sel)

    newout = sorted(out+isoWPs, key=lambda significance: significance[1], reverse=True)
    
    printOutTable(newout, "PLIVWP_table_ranked_" +sel)

    cut_idx = min(len(newout), 4)
    outforslides = newout[:cut_idx] + newisoWPs
    printOutTable(outforslides, "PLIVWP_isoWP_" +sel)

    print(newout[0])
    
    plotPLIVSum(out, 'PLIVWP_sum_' +sel)

    return(isoWPs, out)
 
#======================================================================================================        
def plot():

    zjets_file     = args[0]
    tth_ttbar_file = args[1]

    if not os.path.isfile(zjets_file):
        log.warning('Input file does not exist: %s' %zjets_file)
        return    

    if not os.path.isfile(tth_ttbar_file):
        log.warning('Input file does not exist: %s' %tth_ttbar_file)
        return    

    zjets     = h5py.File(zjets_file,     'r')
    tth_ttbar = h5py.File(tth_ttbar_file, 'r')

    log.info('%s contains %d dataset keys' %(zjets, len(zjets.keys())))
    log.info('%s contains %d dataset keys' %(tth_ttbar_file, len(tth_ttbar.keys())))

    maxl = len(max(tth_ttbar.keys(), key=len))
    
    for k in tth_ttbar.keys():
        d = tth_ttbar[k]        
        log.info('   %s len=%6d, size=%6d, dtype=%8s, shape=%s' %(('%s' %k).ljust(maxl), len(d), d.size, d.dtype, d.shape))

    #
    # Prepare DataFrame
    #
    zjets_df = pd.DataFrame(zjets['train_extra_data'][:], columns = zjets['extra_vars'][:])
    tth_all_df = pd.DataFrame(tth_ttbar['train_extra_data'][:], columns = tth_ttbar['extra_vars'][:])
    tth_all_df['label'] = tth_ttbar['train_labels'][:]
    event_leps = tth_ttbar['train_lep_data'][:]
    zjet_leps  = zjets['train_lep_data'][:]
    
    # prepare prompt leptons from ZJets for PLIV cut value calculation
    lep_data = pd.DataFrame()
    lep_data['pt']   = pd.concat([zjets_df['Lep0Pt'], zjets_df['Lep1Pt']])
    lep_data['pliv'] = pd.concat([zjets_df['Lep0PLIV'], zjets_df['Lep1PLIV']])

    # isolation WPs 
    lep_var_dict = {var: i for i, var in enumerate(tth_ttbar['lep_train_vars'][:])}
    for iso in ['m_leps_isoFCLoose', 'm_leps_isoFCTight', 
                'm_leps_isoFixedCutPflowLoose', 'm_leps_isoFixedCutPflowTight', 
                'm_leps_isoPLVLoose', 'm_leps_isoPLVTight']:
        iso_nick = iso.replace('m_leps_iso', '')
        tth_all_df['Lep0_'+iso_nick] = event_leps[:, 0, lep_var_dict[iso]]
        tth_all_df['Lep1_'+iso_nick] = event_leps[:, 1, lep_var_dict[iso]]

        lep_data[iso_nick] = np.concatenate([zjet_leps[:, 0, lep_var_dict[iso]], zjet_leps[:, 1, lep_var_dict[iso]]], axis = 0)

    
    isos = ['FCTight', 'FixedCutPflowTight', 'PLVLoose','PLVTight',]
    #isos = ['FCLoose','FCTight', 'FixedCutPflowLoose', 'FixedCutPflowTight', 'PLVLoose','PLVTight',]
    # split into different categories
    ttbar_df = tth_all_df[tth_all_df['label'] == 4]

    edges = [10e3, 15e3, 20e3, 25e3, 32e3, 43e3]

    effs_cands  = []
    
    with open(args[2], 'r') as reader: 
        top_PLIVWP = 4# first line with valid WP 
        wp_items = []

        while len(wp_items) < 6:
            line = reader.readlines()[top_PLIVWP]
            wp_items =  [item.lstrip(' ') for item in line.rstrip(" \\\\ \n").split('&')]
        
        bin_effs = [float(eff)/100 for eff in wp_items[0].split('\_')[1:]]  
    
        effs_cands += [bin_effs]

    print("The best PLIV WP with the efficiency: ", effs_cands[0])
    
    is_best = True
    for effs in effs_cands:

        eff_name = "PLIVWP"
        for eff in effs:
            eff_name += '_%d'%(eff*100)

        pliv_wp = PLIVWP(lep_data, edges, effs)
        plotPLIVCut(pliv_wp, eff_name, useShortName=is_best)
        plotWPEffRej(lep_data, pliv_wp, eff_name + "_prompt", isos, ptcuts=None, useShortName=is_best)

        # prepare non-prompt
        lep_nonprompt = pd.DataFrame()
        lep_nonprompt['pt']   = pd.concat([ttbar_df['Lep0Pt'], ttbar_df['Lep1Pt']])
        lep_nonprompt['pliv'] = pd.concat([ttbar_df['Lep0PLIV'], ttbar_df['Lep1PLIV']])
        lep_nonprompt['type'] = pd.concat([ttbar_df['Lep0AnpTruthType'], ttbar_df['Lep1AnpTruthType']])
        lep_nonprompt['type'] = pd.concat([ttbar_df['Lep0AnpTruthType'], ttbar_df['Lep1AnpTruthType']])
        
        
        for iso in isos:
            lep_nonprompt[iso] = pd.concat([ttbar_df['Lep0_' + iso], ttbar_df['Lep1_'+iso]])

        lep_nonprompt = lep_nonprompt[(lep_nonprompt['type'] == 4) | (lep_nonprompt['type'] == 5)]
#        lep_ttbarprompt = lep_nonprompt[(lep_nonprompt['type'] == 24) | (lep_nonprompt['type'] == 23)]

        plotWPEffRej(lep_nonprompt,   pliv_wp, eff_name+"_nonprompt",   isos, ptcuts=edges, useShortName=is_best)
#        plotWPEffRej(lep_ttbarprompt, pliv_wp, eff_name+"_ttbarprompt", isos, ptcuts=None,  useShortName=is_best)
        
            
#======================================================================================================        
def plotDistribution():

    all_line = ""
    with open(args[0], 'r') as reader:
        all_line = reader.readlines()[-3]
    
    all_events = [item.lstrip(' ')  for item in all_line.rstrip(" \\\\ \n").split('&')]
    all_events[1] = 1

    wps = []
    with open(args[1], 'r') as reader:
        for line in reader.readlines():
            wp_items =  [item.lstrip(' ') for item in line.rstrip(" \\\\ \n").split('&')]

            if not wp_items[0].count('PLIVWP'): continue
            
            bin_effs = [float(eff)/100 for eff in wp_items[0].split('\_')[1:]]
            eff_items = [float(wp_items[i])/float(all_events[i]) for i in range(1, len(wp_items))]
                
            do_skip = False
            for eff_item in eff_items: 
                if math.isnan(eff_item): do_skip = True
                    
            if do_skip: continue

            wps.append(eff_items + bin_effs)
  
    if options.do_wh:  
        columns =['significance', 'sigeff_ratio', 'sigeff', 'bkgeff', 'ttbareff', 'wzeff']
    
    elif options.do_ssww:
        columns =['significance', 'sigeff_ratio', 'sigeff', 'bkgeff', 'ttbareff', 'wzeff', 'qcdsswweff']

    else:
        columns =['significance', 'sigeff_ratio', 'sigeff', 'bkgeff', 'ttbareff', 'wzeff', 'ttweff']
    
    columns += ['bin1eff', 'bin2eff', 'bin3eff', 'bin4eff', 'bin5eff', 'bin6eff']
    
    wps_df = pd.DataFrame(wps, columns=columns)
    print(wps_df)


    #
    # Make plots
    #
    for var in columns[2:]:
        x = wps_df[var].values
        y = wps_df['significance'].values 
        heatmap, xedges, yedges = np.histogram2d(x, y, bins=50)
        extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]

        plt.clf()
        plt.imshow(heatmap.T, extent=extent, origin='lower',  cmap=plt.get_cmap('Blues'), aspect='auto')
        plt.colorbar()
        
        plt.ylabel('significance')
        plt.xlabel(var)

        plt.savefig('%s.pdf' %getOutName('signi_vs_%s'%var), format='pdf')


#======================================================================================================        
if __name__ == '__main__':

    timeStart = time.time()

    log.info('Start job at %s:%s' %(socket.gethostname(), os.getcwd()))
    log.info('Current time: %s' %(time.asctime(time.localtime())))
    
    if options.plot:
        plot()
    elif options.plotDist:
        plotDistribution()
    else:
        main()

    log.info('Local time: %s' %(time.asctime(time.localtime())))
    log.info('Total time: %.1fs' %(time.time()-timeStart))
