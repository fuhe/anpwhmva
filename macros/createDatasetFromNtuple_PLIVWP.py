#!/usr/bin/env python

import re
import os
import subprocess
import sys
import socket
import time
import logging

import uproot
import h5py
import numpy as np

from optparse import OptionParser
p = OptionParser()

p.add_option('-o', '--outfile',  type='string',     default=None)
p.add_option('--tree-name',      type='string',     default='events')

p.add_option('-n', '--nevent',   type='int',                         default=None)
p.add_option('-d', '--debug',    action='store_true',  dest='debug', default=False)

(options,args) = p.parse_args()

#======================================================================================================        
def getLog(name, level='INFO', debug=False, print_time=False):

    if print_time:
        f = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
    else:
        f = logging.Formatter('%(message)s')
        
    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(f)
    
    log = logging.getLogger(name)
    log.addHandler(sh)
    
    if debug:
        log.setLevel(logging.DEBUG)
    else:
        if level == 'DEBUG':   log.setLevel(logging.DEBUG)
        if level == 'INFO':    log.setLevel(logging.INFO)
        if level == 'WARNING': log.setLevel(logging.WARNING)    
        if level == 'ERROR':   log.setLevel(logging.ERROR)

    return log

#======================================================================================================        
log = getLog(os.path.basename(__file__), print_time=True)

#======================================================================================================
def getInputVars():
    
    input_vars = ['Event',
                  'MCChannel',
                  'Lep0Pt',
                  'Lep1Pt',
                  'Lep0PLIV',
                  'Lep1PLIV',
                  'Lep0AnpTruthType',
                  'Lep1AnpTruthType',
                  ]

    return sorted(input_vars)

#======================================================================================================
def getTrainVars():

    '''
    List of training variables 
    - do not use this function to get input variables!
    - please use VarMan.GetTrainVars() to get correctly sorted list
    ''' 
    
    train_vars = [
                  'Lep0Pt',
                  'Lep1Pt',
                  'Lep0PLIV',
                  'Lep1PLIV',
                  'Lep0AnpTruthType',
                  'Lep1AnpTruthType',
                  ]  

    return train_vars

#======================================================================================================
class VarMan:

    '''Class to map between variable index in numpy array and variable name'''
    
    def __init__(self, input_vars, train_vars):

        for v in train_vars:
            if v not in input_vars:
                raise Exception('VarMan - train var "%s" is not on input var list' %v)
               
        self.train_vars = sorted(train_vars)
        self.extra_vars = sorted([x for x in input_vars if x not in train_vars])
        self.input_vars = self.train_vars + self.extra_vars

        self.bytes_read_event_attrs = ['']
        
        self.bytes_dict_train_vars = {}
        self.bytes_dict_input_vars = {}
        self.index_dict_input_vars = {}

        for i in range(len(self.train_vars)):
            self.bytes_dict_train_vars[self.train_vars[i].encode()] =  i

        for i in range(len(self.input_vars)):
            self.bytes_dict_input_vars[self.input_vars[i].encode()] =  i
            self.index_dict_input_vars[i] = self.input_vars[i].encode()             

        self.PrintVars()
            
    def VarToBytes(self, varname):
        if type(varname) == str:
            return varname.encode()
        elif type(varname) == bytes:
            return varname
        elif type(varname) == int:
            return self.index_dict_input_vars[i]
        
        raise Exception('VarMan::VarToBytes - unknown variable type: %s' %varname)

    def GetTrainVars(self):
        return self.train_vars        
    
    def GetNTrainVars(self):
        return len(self.train_vars)
    
    def IsTrainVar(self, varname):        
        return self.VarToBytes(varname) in self.bytes_dict_train_vars

    def GetVarIndex(self, varname):
        return self.bytes_dict_input_vars[self.VarToBytes(varname)]

    def PrintVars(self):
        log.info('VarMan - %d input vars: %s' %(len(self.input_vars), self.input_vars))
        log.info('VarMan - %d train vars: %s' %(len(self.train_vars), self.train_vars))
        log.info('VarMan - %d extra vars: %s' %(len(self.extra_vars), self.extra_vars))

        for v, i in self.bytes_dict_input_vars.items():
            log.info('   index %2d - %s' %(i, v.decode())) 
        
#======================================================================================================
varMan = VarMan(getInputVars(), getTrainVars())    
        
#======================================================================================================
class Event:

    truth_mc   = ('MCChannel').encode()
    event_bvar = ('Event'    ).encode()
    
    def __init__(self, data, index):

        '''Class to hold event data and provide useful functions'''
        
        self.index      = index
        self.train_data = np.zeros(varMan.GetNTrainVars())
        
        for key, value in data.items():
            #
            # Save training variable to numpy array
            #
            if varMan.IsTrainVar(key):
                self.train_data[varMan.GetVarIndex(key)] = value[index]

        self.Event      = data[Event.event_bvar][index]
        self.MCChannel  = data[Event.truth_mc  ][index]
        
        self.event_label        = self.ComputeEventLabel()

    def ComputeEventLabel(self):
        if self.MCChannel in [410470]:
            # background, ttbar
            return 0
                
        if self.MCChannel in [346345, 346344, 346343]:
            # signal, ttH dilep
            return 1

        if self.MCChannel in [361107]:
            # ref, z os mm
            return 2

        return -99

    def GetTrainData(self):
        return self.train_data

    def GetEventIndex(self):
        return self.index
    
    def GetEventLabel(self):
        return self.event_label

    def IsTrainEvent(self):
        # label all event as train
        return True

    def IsTestEvent(self):
        return not self.IsTrainEvent()

    def EventAsStr(self):
        s  = 'Event index=%d' %self.GetEventIndex()
        s += ', label=%d'     %self.GetEventLabel()
        s += ', is_train= %d' %self.IsTrainEvent()

        return s
    
#======================================================================================================        
def exploreFile(fpath, branches):
    
    if not os.path.isfile(fpath):
        log.warning('exploreFile - invalid file: %s' %fpath)
        return 

    fdata = uproot.open(fpath)

    if options.tree_name.encode() not in fdata:
        log.warning('exploreFile - file: %s is missing tree "%s"' %(fpath,options.tree_name ))
        return 

    data = fdata[options.tree_name.encode()]

    log.info('Print data: %s' %data)
    log.info('Print dir(data): %s' %dir(data))
    log.info('Print keys: %s' %data.keys())
    data.show()

    icount = 0

    branches = getInputVars()
    
    for dvals in data.iterate(branches, entrystart=0, entrystop=options.nevent, entrysteps=None):
        icount += 1
        
        for key, data in dvals.items():    
            log.info('key=%s, len=%d, %s' %(key, len(data), data))

    log.info('icount=%d' %icount)

#======================================================================================================        
def readFileData(fpath, branches):
    
    if not os.path.isfile(fpath):
        log.warning('readFile - invalid file: %s' %fpath)
        return 

    fdata = uproot.open(fpath)

    if options.tree_name.encode() not in fdata:
        log.warning('readFileData - file: %s is missing tree "%s"' %(fpath,options.tree_name ))
        return 

    utree = fdata[options.tree_name.encode()]
   
    ncount = 0

    for evt_data in utree.iterate(branches=branches, entrystop=options.nevent, entrysteps=float("inf")):
        ncount += 1
        log.info('N iterator = %s'%ncount)

        len_data = 0
        
        for key, data in evt_data.items():
        
            branch = key.decode('utf-8')
            log.info('Branch %5d entries for branch: %s' %(len(data), branch))
            
            if len_data == 0:
                len_data = len(data)
            elif len_data != len(data):
                raise Exception('readFile - logic error: branch "%s" have different length: %d != %d' %(branch, len_data, len(data)))

    log.info('readFile - all is done for %s' %fpath)

    return (len_data, evt_data)

#======================================================================================================        
def prepNumpyArrays(events):

    '''Prepare training and test numpy arrays'''

    train_events = []

    for e in events:
        if e.GetEventLabel() < -1:
            continue
        
        if   e.IsTrainEvent(): train_events += [e]
    
    #
    # Create training arrays
    #
    train_data   = np.zeros((len(train_events), varMan.GetNTrainVars()))
    train_labels = np.zeros( len(train_events), dtype=np.int8)
    
    for i in range(len(train_events)):
        event = train_events[i]
        
        train_data[i]   = event.GetTrainData()
        train_labels[i] = event.GetEventLabel()

        if options.debug:
            log.info(event.EventAsStr())
            log.info(event.GetTrainData())
            log.info(train_data[i])        


    log.info('Size of training data: %d' %train_data.size)

    #
    # Put results into dictionary
    #
    result = {}

    result['train_data']   = train_data
    result['train_labels'] = train_labels

    return result
    
#======================================================================================================        
def main():

    if len(args) != 1:
        log.warning('Wrong number of command line arguments: %s' %len(args))
        return

    if options.debug:
        exploreFile(args[0], getInputVars())
    
    len_data, data = readFileData(args[0], getInputVars())

    log.info('main - read %d data entries' %len_data)
    
    events = []

    for i in range(0, len_data):
        if options.nevent and i >= options.nevent:
            break
        
        events += [Event(data, i)]

        if i % 10000 == 0:
            log.info('Processing event #%7d' %i)

    log.info('main - prepared %d events' %len(events))
            
    result = prepNumpyArrays(events)
    
    if options.outfile:
        #
        # Save training and testing data, and names of training variables saved at matching array index
        #
        log.info('main - saving results to: %s' %options.outfile)
        
        ofile = h5py.File(options.outfile, 'w')

        for key, data in result.items():
            ofile[key] = data

        train_vars = varMan.GetTrainVars()
            
        dset = ofile.create_dataset('train_vars', (len(train_vars),), dtype=h5py.special_dtype(vlen=str))

        for i in range(len(train_vars)):
            dset[i] = train_vars[i]
            
#======================================================================================================        
if __name__ == '__main__':

    timeStart = time.time()

    log.info('Start job at %s:%s' %(socket.gethostname(), os.getcwd()))
    log.info('Current time: %s' %(time.asctime(time.localtime())))
    
    main()

    log.info('Local time: %s' %(time.asctime(time.localtime())))
    log.info('Total time: %.1fs' %(time.time()-timeStart))
