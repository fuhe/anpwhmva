#!/usr/bin/env python

import os
import socket
import sys
import time
import logging
import math

import h5py
import numpy as np
import pandas as pd

#======================================================================================================        
def prepareOptionParser():
    from optparse import OptionParser

    p = OptionParser()
    
    p.add_option('-d', '--debug',    action='store_true', default=False)
    p.add_option('--plot',           action='store_true', default=False)
    p.add_option('--plotDist',       action='store_true', default=False)
    p.add_option('--do-sys30',       action='store_true', default=False)
    p.add_option('--do-sys50',       action='store_true', default=False)
    p.add_option('--do-wh',          action='store_true', default=False)
    p.add_option('--do-ssww',        action='store_true', default=False)
    p.add_option('--do-tth',         action='store_true', default=False)
    p.add_option('--do-ttw',         action='store_true', default=False)
    p.add_option('--do-compare',     action='store_true', default=False)
    p.add_option('--do-wp',          action='store_true', default=False)
    p.add_option('--do-ptraw',       action='store_true', default=False)
    p.add_option('--do-ptbin',       action='store_true', default=False)
    p.add_option('--finetune',       action='store_true', default=False)
    
    p.add_option('--do-elec-fwd',    action='store_true', default=False)
    p.add_option('--do-elec-cen',    action='store_true', default=False)
    
    p.add_option('--no-ECID',        action='store_true', default=False)
    
    p.add_option('--FCLoose',        action='store_true', default=False)
    p.add_option('--lep-veto',       action='store_true', default=False)
    
    p.add_option('-n', '--nevent',   type='int',    default=None)
    p.add_option('-o', '--outdir',   type='string', default='models/')
    p.add_option('--outkey',         type='string', default='mlp')
    p.add_option('--scaler',         type='string', default=None)
    
    p.add_option('--highpt',         type='int',    default=None)
    p.add_option('--binsize',        type='int',    default=1e3)
    
    (options, args) = p.parse_args()
    return (options, args)

#======================================================================================================        
(options, args) = prepareOptionParser()

#======================================================================================================        
def getLog(name, level='INFO', debug=False, print_time=False):

    if print_time:
        f = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
    else:
        f = logging.Formatter('%(message)s')
        
    sh = logging.StreamHandler()
    sh.setFormatter(f)

    logging._warn_preinit_stderr = 0
    
    log = logging.getLogger(name)

    log.handlers  = []
    log.propagate = False
    
    log.addHandler(sh)
    
    if debug:
        log.setLevel(logging.DEBUG)
    else:
        if level == 'DEBUG':   log.setLevel(logging.DEBUG)
        if level == 'INFO':    log.setLevel(logging.INFO)
        if level == 'WARNING': log.setLevel(logging.WARNING)    
        if level == 'ERROR':   log.setLevel(logging.ERROR)

    return log

#======================================================================================================        
log = getLog(os.path.basename(__file__), print_time=False)

#======================================================================================================
def getOutName(name):
    
    if not options.outdir:
        return None
    
    outdir = '%s/' %(options.outdir.rstrip('/'))
    
    if options.highpt:
        outdir = outdir + "Pt%d"%options.highpt 

    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    return '%s/%s' %(outdir.rstrip('/'), name)

#======================================================================================================        
def getEffPLIVCut(lep_data, eff, pT, isleft=False, binsize=1000):
    index = [False]*len(lep_data)

    pt = 'pt'
    if options.do_ptraw or options.do_ptbin:
         pt = 'ptraw'

    if isleft:
        index = (lep_data[pt] > (pT-binsize)) & (lep_data[pt] < pT)
    else:
        index = (lep_data[pt] < (pT+binsize)) & (lep_data[pt] > pT)

    #lep_data[index]['pliv'][lep_data[index]['pliv'] > -1.1].plot(kind='hist', bins=200)
    pliv_list = lep_data[index]['pliv'].values
    total_leps_bin = len(pliv_list)
    
    if total_leps_bin == 0:
        return -99.

    id_of_eff = round(total_leps_bin*eff)
    return np.sort(pliv_list)[id_of_eff]
    
    
#====================================================================================================== 
class Bin:
    def __init__(self, leps, bin_left, left_eff, bin_right=100e3, right_eff=None):
        self.bin_left  = bin_left
        self.bin_right = bin_right
        
        if leps is None:
            self.left_cut  = left_eff
            self.right_cut = right_eff
        
        else:
            # cut pliv value
            self.left_cut = getEffPLIVCut(leps, left_eff, bin_left, isleft=False, binsize=options.binsize)
            
            if bin_right and right_eff:
                self.right_cut = getEffPLIVCut(leps, right_eff, bin_right, isleft=True, binsize=options.binsize)
            else:
                self.right_cut = self.left_cut      
    
    def getPLIVCut(self, pT):
        k = (self.right_cut - self.left_cut)/(self.bin_right - self.bin_left)
        b = self.right_cut - self.bin_right*k
        
        return k*pT+b
    
    def isInside(self, pT):
        if pT >= self.bin_left and pT < self.bin_right:
            return True
        
        return False
        

#====================================================================================================== 
class PLIVWP:
    def __init__(self, leps_ref, edges, effs, doMVABins = options.do_ptbin):
        self.do_ptbin = doMVABins
        self.maxEdge = edges[len(edges) -1]
        self.bins = []
        
        if len(edges) == len(effs):        
            for i in range(len(edges)):
                if self.do_ptbin:
                    leps = leps_ref[leps_ref['ptbin'] == i+1]
                else:
                    leps = leps_ref

                if i+1 >= len(edges):
                    cur_bin = Bin(leps, edges[i], effs[i])
                else:
                    cur_bin = Bin(leps, edges[i], effs[i], edges[i+1], effs[i+1])
                
                self.bins += [cur_bin]

        elif len(effs) == 2 and len(edges) == len(effs[0]):
            left_cuts  = effs[0]
            right_cuts = effs[1]

            for i in range(len(edges)):
                if i+1 >= len(edges):
                    cur_bin = Bin(None, edges[i], left_cuts[i], 100e3, right_cuts[i])
                else:
                    cur_bin = Bin(None, edges[i], left_cuts[i], edges[i+1], right_cuts[i])

                self.bins += [cur_bin]
        
    def getFullPLIVCut(self, pT):
        if pT >= self.maxEdge:
            return self.bins[-1].getPLIVCut(pT)
        
        if pT <= self.bins[0].bin_left:
            return self.bins[0].getPLIVCut(pT)

        for cur_bin in self.bins:
            if cur_bin.isInside(pT):
                return cur_bin.getPLIVCut(pT)
       
    def getFullPLIVBinCut(self, lep):
        if lep['ptbin'] -1 < 0 or not self.do_ptbin:
            # those lepton do not fill PLIV relate variables, due to no track-jet close to it.
            return self.getFullPLIVCut(lep['pt'])

        return self.bins[int(lep['ptbin']) -1].getPLIVCut(lep['pt'])

    def printPLIVCut(self):
        left_cuts  = [] 
        right_cuts = [] 

        for i, cur_bin in enumerate(self.bins):
            left_cuts  += [float("{:.4f}".format(cur_bin.left_cut))]
            right_cuts += [float("{:.4f}".format(cur_bin.right_cut))]
            print("Bin", i, " left cut =", cur_bin.left_cut, ", right cut = ", cur_bin.right_cut)

        print(left_cuts)
        print(right_cuts)

        return 0

